# Changelog

## v2.5.3 (2025-03-04)

### Fixed

- Updated to PMD [v7.11.0](https://github.com/pmd/pmd/releases/tag/pmd_releases/7.11.0),
  which has no CPD changes.
- Update to Go v1.24.1.

## v2.5.2 (2025-02-11)

### Fixed

- Updated base image to latest `eclipse-temurin:21.0.6_7-jre-jammy` security
  patch.
- Update to Go v1.23.6.

## v2.5.1 (2025-02-04)

### Fixed

- Updated to PMD [v7.10.0](https://github.com/pmd/pmd/releases/tag/pmd_releases/7.10.0),
  which has no CPD changes.
- Updated base image to latest `eclipse-temurin:21.0.6_7-jre-jammy`.
- Update to Go v1.23.5.

## v2.5.0 (2024-12-28)

### Changed

- Updated to PMD [v7.9.0](https://github.com/pmd/pmd/releases/tag/pmd_releases/7.9.0),
  which added CPD support for Rust.

### Fixed

- Update to Go v1.23.4.

## v2.4.1 (2024-11-29)

### Changed

- Updated to PMD [v7.8.0](https://github.com/pmd/pmd/releases/tag/pmd_releases/7.8.0),
  which fixed several issues.

### Fixed

- Update to Go v1.23.3.

## v2.4.0 (2024-10-25)

### Changed

- Updated to PMD [v7.7.0](https://github.com/pmd/pmd/releases/tag/pmd_releases/7.7.0),
  which allows ignoring literals and identifiers in C++ (using CLI options
  `--ignore-literal` and `--ignore-identifiers`).

### Fixed

- Updated container base image to `eclipse-temurin:21.0.5_11-jre-jammy`.
- Update to Go v1.23.2.

## v2.3.6 (2024-09-27)

### Fixed

- Updated to PMD [v7.6.0](https://github.com/pmd/pmd/releases/tag/pmd_releases/7.6.0)
  (no functional PMD CPD changes).
- Fixed CI pipeline to update image `annotations` to be correct for this
  project. Previously, the `annotations` from the base `eclipse-temurin` image
  were cascading to this image.

### Miscellaneous

- Updated Renovate config to remove deprecated `excludeDepNames` setting.

## v2.3.5 (2024-09-21)

### Fixed

- Updated output file permissions to `rw` for the current user only.
- Updated base image to latest `eclipse-temurin:21.0.4_7-jre-jammy`.
- Update to Go v1.23.1.

## v2.3.4 (2024-08-30)

### Fixed

- Updated to PMD v7.5.0 (no functional PMD CPD changes).
- Updated base image to latest `eclipse-temurin:21.0.4_7-jre-jammy`.

## v2.3.3 (2024-08-18)

### Fixed

- Update to Go v1.23.0.
- Updated base image to latest `eclipse-temurin:21.0.4_7-jre-jammy`.

## v2.3.2 (2024-07-26)

### Fixed

- Updated to PMD v7.4.0, which fixes a false `skipLexicalErrors` error message.
  The complete list of PMD CPD fixes can be found in the
  [release notes](https://github.com/pmd/pmd/releases/tag/pmd_releases%2F7.4.0).
- Updated image to `eclipse-temurin:21.0.4`.
- Update to Go v1.22.5.

## v2.3.1 (2024-06-28)

### Fixed

- Updated to PMD v7.3.0 with the following changes (no functional changes).
  The complete list of PMD CPD fixes can be found in the
  [release notes](https://github.com/pmd/pmd/releases/tag/pmd_releases%2F7.3.0).
  - Updated the `pmd-cpd.sh` script to replace the `--skip-lexical-errors=true`
    CLI argument with `--no-fail-on-error` as this argument was deprecated in
    v7.3.0 (it still works, but will give a warning).
  - Updated all test examples to the latest PMD CPD report schema, which is now
    formally versioned with an XSD schema.

## v2.3.0 (2024-05-31)

### Changed

- Updated to PMD v7.2.0. See the
  [release notes](https://github.com/pmd/pmd/releases/tag/pmd_releases/7.2.0)
  for details.

## v2.2.1 (2024-05-13)

### Fixed

- Refactored for changes in Go 1.22.
- Update to Go v1.22.3.
- Updated to `golangci-lint` v1.58.0 and latest lint config.

### Miscellaneous

- Updated to Renovate config v1.0.1.

## v2.2.0 (2024-04-25)

### Changed

- Updated to PMD v7.1.0, with improvements for overlapping duplicates reported
  by PMD CPD. See the
  [release notes](https://github.com/pmd/pmd/releases/tag/pmd_releases/7.1.0)
  for details.

## v2.1.2 (2024-04-24)

### Fixed

- Updated image to `eclipse-temurin:21.0.3`.

## v2.1.1 (2024-04-03)

### Fixed

- Update to `golang:1.22.2`.

## v2.1.0 (2024-03-23)

### Changed

- Updated to final PMD v7.0.0 (no specific changes for `gitlab-pmd-cpd`).

### Miscellaneous

- Added end-to-end tests and merged code coverage results. (#24)

## v2.0.3 (2024-03-05)

### Fixed

- Update to `golang:1.22.1`.

### Miscellaneous

- Updated Renovate config to use use new [presets](https://gitlab.com/gitlab-ci-utils/renovate-config)
  project. (#23)

## v2.0.2 (2024-02-14)

### Fixed

- Update to Java 21.0.2.
- Update to `golang:1.22.0`.

## v2.0.1 (2024-01-19)

### Fixed

- Update to `golang:1.21.6`.

### Miscellaneous

- Updated Renovate config to manage Alpine versions for `golang` container
  image.
- Incorporate latest GitLab CI templates for Go.

## v2.0.0 (2023-11-18)

### Changes

- BREAKING: Migrated application from JavaScript to Go. There are no planned
  changes other than additional console output detailing the conversion
  process. (#18)
- BREAKING: Corrected Code Quality report schema to output `content.tokens`
  as a number instead of a string. Only breaking if it were being parsed
  specifically as a string (it is not used by GitLab). (#20)
- BREAKING: Changed variables `PMDCPD_DIR` to `PMDCPD_DIR_FILES` which
  indicates either the directory (e.g. `PMDCPD_DIR_FILES="--dir=."`) or file
  list (e.g. `PMDCPD_DIR_FILES='--file-list=files.txt'`) to scan for
  duplicates. Default in `pmd-cpd.sh` is to scan the current directory.
  Since `--dir` only specifies a directory, the `--file-list` option can be
  used when required to specify a list of files to scan, excluding some
  files (e.g. in Go, excluding `*_test.go` files).
- BREAKING: Updated container image to Java 21 (to run PMD CPD).

### Miscellaneous

- Updated `image_test` CI job to check contents of the GitLab Code Quality
  results generated in the image. (#22)
- Added `.devcontainer.json` file with Go configuration for development.
- Added script to generate the example PMD CPD XML results files used for
  testing.

## v1.1.0 (2023-10-01)

### Changed

- Updated to PMD v7.0.0-rc4 (no specific changes for `gitlab-pmd-cpd`).

### Miscellaneous

- Updated documentation with link to all supported languages. (#15)
- Updated NPM package to new standard format. (#16)

## v1.0.0 (2023-08-05)

### Changed

- Updated report to include all duplication locations so they are all
  displayed in the GitLab merge request UI widget. Previously only the first
  occurrence was show and the JSON report had to be referenced to view the
  other locations. (#8)

### Fixed

- Updated to latest dependencies.

## v0.6.0 (2023-06-19)

### Changed

- BREAKING: Updated to PMD v7.0.0-rc3.

### Fixed

- Fixed script to remove shebang from TypeScript files before testing. (#14)

### Miscellaneous

- Updated Renovate config to track PMD versions.

## v0.5.0 (2023-05-25)

Initial release
