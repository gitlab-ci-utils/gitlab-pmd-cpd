#!/bin/bash

# Remove shebang from any JS/TS files, otherwise PMD fails to parse them.
find . -type f \( -name "*.js" -o -name "*.ts" \) -exec sed -i '1s/^#!.*$//' {} \;

# Run PMD CPD with applicable arguments and return XML file with results.
# shellcheck disable=SC2086
pmd cpd \
  ${PMDCPD_DIR_FILES:---dir=.} \
  --minimum-tokens="${PMDCPD_MIN_TOKENS:-100}" \
  --language="${PMDCPD_LANGUAGE:-ecmascript}" \
  --no-fail-on-violation \
  --no-fail-on-error \
  --format="xml" \
  ${PMDCPD_CLI_ARGS} > "${PMDCPD_RESULTS:-pmd-cpd-results.xml}"

# Convert PMD CPD XML to GitLab Code Quality JSON format.
gitlab-pmd-cpd "${PMDCPD_RESULTS:-pmd-cpd-results.xml}"
