# GitLab PMD CPD

A container image to run
[PMD's Copy/Paste Detector (CPD)](https://pmd.github.io/pmd/pmd_userdocs_cpd.html)
and provide results in GitLab Code Quality report format.

**Note: this has been tested on JavaScript, TypeScript, and Go files, some of
which do require some unique treatment. It's possible that other languages may
have problems. Please report any issues that are encountered.**

## Usage

The following example shows how to use this image to execute PMD CPD in the current directory with the default options:

```yaml
image_test:
  image:
    name: registry.gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd:latest
    entrypoint: ['']
  stage: test
  script:
    - /gitlab-pmd-cpd/pmd-cpd.sh
  artifacts:
    paths:
      - pmd-cpd-results.*
    reports:
      codequality: pmd-cpd-results.json
```

The `pmd-cpd.sh` script includes all steps to run PMD CPD and convert the
output to a GitLab Code Quality report. The script can be customized by setting
the following environment variables:

- `PMDCPD_DIR_FILES`: The directory or files to scan for duplicate code. Must
  be one of the options `--dir`, `--file-list` or `--uri` with the appropriate
  value (for example `--file-list=files.txt`). (default: `--dir=.`).
  See the [PMD CPD docs](https://pmd.github.io/pmd/pmd_userdocs_cpd.html#cli-options-reference)
  for details.
- `PMDCPD_MIN_TOKENS`: The minimum number of tokens to consider a code fragment
  as duplicate (default: `100`).
- `PMDCPD_LANGUAGE`: The language to use for duplicate code detection (default:
  `ecmascript`). The complete list of supported languages can be found
  [here](https://pmd.github.io/pmd/pmd_userdocs_cpd.html#supported-languages).
- `PMDCPD_RESULTS`: The name of the PMD CPD results file (default:
  `pmd-cpd-results.xml`). Note that the file extension must be `.xml` for the
  conversion to GitLab Code Quality report format to work properly, and that the
  resulting JSON file uses the same base name (for example
  `pmd-cpd-results.json` for the default name).
- `PMDCPD_CLI_ARGS`: Any additional PMD CPD command line arguments (default:
  none). See the
  [PMD CPD docs](https://pmd.github.io/pmd/pmd_userdocs_cpd.html#cli-usage)
  for additional details.
  - The `--exclude <path>` option can be used to exclude files from the scan.
    This option can be used multiple times.
  - Note that the `--no-fail-on-violation`, `--no-fail-on-error`, and
    `--format="xml"` values are fixed in the script.
- `PMD_JAVA_OPTS`: This is used by PMD to set the Java heap size, for example
  `PMD_JAVA_OPTS=-Xmx1024m` to set the maximum size to 1 GB.

Since the report is declared as a GitLab Code Quality report, it is
displayed in the [Code Quality merge request widget](https://docs.gitlab.com/ee/ci/testing/code_quality.html#merge-request-widget)
as shown below.

![GitLab Merge request widget](docs/gitlab-merge-request-widget.png)

Note that unlike GitLab's built-in Code Quality job, this job shows all
instances of duplicate code in the merge request widget.

## GitLab PMD CPD container images

All available container image tags are found in the
`gitlab-ci-utils/gitlab-pmd-cpd` repository at
https://gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/container_registry.

The following tag conventions are used:

- `latest`: The latest build of the image.
- `X.Y.Z`: Versioned releases of the image. Details on each release can be
  found on the [Releases](https://gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/-/releases)
  page.

**Note:** Any other image tags in the `gitlab-ci-utils/gitlab-pmd-cpd`
repository are images from feature branches. Any images in the
`gitlab-ci-utils/gitlab-pmd-cpd/tmp` repository are temporary images used
during the build process. Any of these images may be deleted at any time.

## License

PMD is licensed under the Apache-2.0 and BSD-4-Clause licenses. See the
`/pmd-bin-*/LICENSE` file in the image for additional details. The source code
for PMD is available from the
[PMD repository](https://github.com/pmd/pmd/releases). This project, including
the Dockerfile and Go application to convert the PMD CPD XML output to GitLab
Code Quality report format, is licensed under the
[Apache-2.0 license](LICENSE). See the `/gitlab-pmd-cpd/LICENSE` file in the
image for additional details.
