package main

import (
	"os"
	"strings"

	"gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/internal/app"
	"gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/internal/logger"
)

func main() {
	log := &logger.DefaultLogger{}
	const reqArgs = 2
	if len(os.Args) < reqArgs {
		log.Fatalln("Error: PMD CPD XML report file name must be specified as the first argument")
	}
	pmdcpdFilename := os.Args[reqArgs-1]
	if !strings.HasSuffix(pmdcpdFilename, ".xml") {
		log.Fatalln("Error: PMD CPD results file must be XML, given filename: ", pmdcpdFilename)
	}
	codeQualityFilename := strings.Replace(pmdcpdFilename, ".xml", ".json", 1)

	if err := app.Run(pmdcpdFilename, codeQualityFilename, log); err != nil {
		log.Fatalln(err)
	}
}
