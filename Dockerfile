FROM golang:1.24.1-alpine3.21@sha256:43c094ad24b6ac0546c62193baeb3e6e49ce14d3250845d166c77c25f64b0386 as build

WORKDIR /src
COPY . .
RUN env GOOS=linux GOARCH=amd64 go build -o gitlab-pmd-cpd ./cmd/gitlab-pmd-cpd/

FROM eclipse-temurin:21.0.6_7-jre-jammy@sha256:02fc89fa8766a9ba221e69225f8d1c10bb91885ddbd3c112448e23488ba40ab6 as final

ARG PMD_VERSION

COPY --from=build /src/gitlab-pmd-cpd /src/LICENSE /src/pmd-cpd.sh /gitlab-pmd-cpd/

# hadolint ignore=DL3008
RUN apt-get update && \
  apt-get -y install --no-install-recommends unzip && \
  rm -rf /var/lib/apt/lists/* && \
  wget -q https://github.com/pmd/pmd/releases/download/pmd_releases/${PMD_VERSION}/pmd-dist-${PMD_VERSION}-bin.zip && \
  unzip pmd-dist-${PMD_VERSION}-bin.zip && \
  rm -f pmd-dist-${PMD_VERSION}-bin.zip && \
  chmod +x /gitlab-pmd-cpd/pmd-cpd.sh

ENV PATH=$PATH:/pmd-bin-${PMD_VERSION}/bin:/gitlab-pmd-cpd

ENTRYPOINT [ "pmd" ]
CMD [ "cpd", "-h" ]

LABEL org.opencontainers.image.licenses="Apache-2.0 AND BSD-4-Clause" \
  org.opencontainers.image.source="https://gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd" \
  org.opencontainers.image.title="pmd" \
  org.opencontainers.image.url="https://gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd"
