//nolint:funlen,gocognit // Don't test function length or complexity in tests
package app_test

import (
	"bytes"
	"fmt"
	"log" //nolint:depguard // Will move to slog with #21.
	"os"
	"path"
	"strings"
	"testing"

	"gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/internal/app"
	"gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/internal/files"
	"gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/internal/logger"
	"gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/internal/testdata"
)

type testCase struct {
	testName           string
	reportTestName     string
	pmdcpdFile         string
	codeQualityFile    string
	expCodeQualityFile string
	expFatal           []string
	expPrint           []string
	hasError           bool
}

// filename should be relative to project root.
func getFullPath(filename string) string {
	wd, err := os.Getwd()
	if err != nil {
		panic("Error getting working directory")
	}
	return path.Join(wd, "..", "..", filename)
}
func TestRun(t *testing.T) {
	testCases := []testCase{
		{
			testName:           "should convert a PMD CPD file with a single duplication",
			reportTestName:     "should produce the correct Code Quality report with a single duplication",
			pmdcpdFile:         getFullPath("internal/testdata/pmdcpd/single-duplication.xml"),
			codeQualityFile:    getFullPath("internal/testdata/pmdcpd/single-duplication.json"),
			expCodeQualityFile: getFullPath("internal/testdata/codequality/single-duplication.json"),
			expFatal:           []string{},
			expPrint: []string{
				fmt.Sprintf("Read file: %s",
					getFullPath("internal/testdata/pmdcpd/single-duplication.xml")),
				"PMD CPD analyzed 4 files, found 1 duplications",
				fmt.Sprintf("Converting file %s to GitLab Code Quality JSON",
					getFullPath("internal/testdata/pmdcpd/single-duplication.xml")),
				fmt.Sprintf("GitLab Code Quality report saved to file: %s",
					getFullPath("internal/testdata/pmdcpd/single-duplication.json")),
			},
			hasError: false,
		},
		{
			testName:           "should convert a PMD CPD file with multiple duplications",
			reportTestName:     "should produce the correct Code Quality report with multiple duplications",
			pmdcpdFile:         getFullPath("internal/testdata/pmdcpd/multiple-duplications.xml"),
			codeQualityFile:    getFullPath("internal/testdata/pmdcpd/multiple-duplications.json"),
			expCodeQualityFile: getFullPath("internal/testdata/codequality/multiple-duplications.json"),
			expFatal:           []string{},
			expPrint: []string{
				fmt.Sprintf("Read file: %s",
					getFullPath("internal/testdata/pmdcpd/multiple-duplications.xml")),
				"PMD CPD analyzed 4 files, found 3 duplications",
				fmt.Sprintf("Converting file %s to GitLab Code Quality JSON",
					getFullPath("internal/testdata/pmdcpd/multiple-duplications.xml")),
				fmt.Sprintf("GitLab Code Quality report saved to file: %s",
					getFullPath("internal/testdata/pmdcpd/multiple-duplications.json")),
			},
			hasError: false,
		},
		{
			testName:           "should convert a PMD CPD file with no duplications",
			reportTestName:     "should produce the correct Code Quality report with no duplications",
			pmdcpdFile:         getFullPath("internal/testdata/pmdcpd/no-duplication.xml"),
			codeQualityFile:    getFullPath("internal/testdata/pmdcpd/no-duplication.json"),
			expCodeQualityFile: getFullPath("internal/testdata/codequality/no-duplication.json"),
			expFatal:           []string{},
			expPrint: []string{
				fmt.Sprintf("Read file: %s",
					getFullPath("internal/testdata/pmdcpd/no-duplication.xml")),
				"PMD CPD analyzed 4 files, found 0 duplications",
				fmt.Sprintf("Converting file %s to GitLab Code Quality JSON",
					getFullPath("internal/testdata/pmdcpd/no-duplication.xml")),
				fmt.Sprintf("GitLab Code Quality report saved to file: %s",
					getFullPath("internal/testdata/pmdcpd/no-duplication.json")),
			},
			hasError: false,
		},
		{
			testName:        "should return an error if the input file does not exist",
			pmdcpdFile:      getFullPath("invalid-file.xml"),
			codeQualityFile: getFullPath("invalid-file.json"),
			expFatal:        []string{},
			expPrint: []string{
				fmt.Sprintf("Error reading file: %s", getFullPath("invalid-file.xml")),
			},
			hasError: true,
		},
		{
			testName:        "should return an error if the input file is not a valid XML file",
			pmdcpdFile:      getFullPath("internal/testdata/pmdcpd/invalid-xml.xml"),
			codeQualityFile: getFullPath("internal/testdata/pmdcpd/invalid-xml.json"),
			expFatal:        []string{},
			expPrint: []string{
				fmt.Sprintf("Read file: %s",
					getFullPath("internal/testdata/pmdcpd/invalid-xml.xml")),
				fmt.Sprintf("Error extracting PMD CPD results from file: %s",
					getFullPath("internal/testdata/pmdcpd/invalid-xml.xml")),
			},
			hasError: true,
		},
		{
			testName:        "should return an error if the input file has an invalid schema",
			pmdcpdFile:      getFullPath("internal/testdata/pmdcpd/other-schema.xml"),
			codeQualityFile: getFullPath("internal/testdata/pmdcpd/other-schema.json"),
			expFatal:        []string{},
			expPrint: []string{
				fmt.Sprintf("Read file: %s",
					getFullPath("internal/testdata/pmdcpd/other-schema.xml")),
				fmt.Sprintf("Error extracting PMD CPD results from file: %s",
					getFullPath("internal/testdata/pmdcpd/other-schema.xml")),
			},
			hasError: true,
		},
		{
			testName:        "should return an error if the output file cannot be written",
			pmdcpdFile:      getFullPath("internal/testdata/pmdcpd/no-duplication.xml"),
			codeQualityFile: getFullPath("invalid/no-duplication.json"),
			expFatal:        []string{},
			expPrint: []string{
				fmt.Sprintf("Read file: %s",
					getFullPath("internal/testdata/pmdcpd/no-duplication.xml")),
				"PMD CPD analyzed 4 files, found 0 duplications",
				fmt.Sprintf("Converting file %s to GitLab Code Quality JSON",
					getFullPath("internal/testdata/pmdcpd/no-duplication.xml")),
				fmt.Sprintf("Error writing file: %s",
					getFullPath("invalid/no-duplication.json")),
			},
			hasError: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.testName, func(t *testing.T) {
			log := &testdata.TestLogger{T: t}

			err := app.Run(tc.pmdcpdFile, tc.codeQualityFile, log)

			if tc.hasError && err == nil {
				t.Error("Expected error, but got none")
			} else if !tc.hasError && err != nil {
				t.Errorf("Expected no error, but got: %v", err)
			}

			if len(tc.expFatal) == 0 && len(log.FatalLog) != 0 {
				t.Error("Expected no errors, got:", strings.Join(log.FatalLog, "\n"))
			} else if !testdata.AreEqualStringSlices(log.FatalLog, tc.expFatal) {
				t.Error("Expected fatal output:\n", strings.Join(tc.expFatal, "\n"),
					"\nGot:\n", strings.Join(log.FatalLog, "\n"))
			}

			if len(tc.expPrint) == 0 && len(log.PrintLog) != 0 {
				t.Error("Expected no messages, got:", strings.Join(log.PrintLog, "\n"))
			} else if !testdata.AreEqualStringSlices(log.PrintLog, tc.expPrint) {
				t.Error("Expected print output:\n", strings.Join(tc.expPrint, "\n"),
					"\nGot:\n", strings.Join(log.PrintLog, "\n"))
			}
		})
	}

	// Run one test with the default logger. Need to check for inclusion of
	// output instead of exact match since logger adds other data to the
	// output.
	defaultLoggerTestCase := testCases[0]
	t.Run(defaultLoggerTestCase.testName+" with default logger", func(t *testing.T) {
		logger := &logger.DefaultLogger{}
		var buf bytes.Buffer
		log.SetOutput(&buf)
		defer func() { log.SetOutput(os.Stderr) }()

		pmdcpdFile := defaultLoggerTestCase.pmdcpdFile
		codeQualityFile := defaultLoggerTestCase.codeQualityFile

		if err := app.Run(pmdcpdFile, codeQualityFile, logger); err != nil {
			t.Errorf("Expected no error, but got %v", err)
		}

		actualOutput := buf.String()
		for _, expectedOutput := range defaultLoggerTestCase.expPrint {
			if !strings.Contains(actualOutput, expectedOutput) {
				t.Errorf("Expected output:\n%s\nGot:\n%s\n", expectedOutput, actualOutput)
			}
		}
	})

	// Get test cases that produce a valid report.
	reportTestCases := []testCase{}
	for _, tc := range testCases {
		if !tc.hasError {
			reportTestCases = append(reportTestCases, tc)
		}
	}

	t.Setenv("CI_PROJECT_DIR", "/builds/gitlab-ci-utils/gitlab-pmd-cpd")
	for _, tc := range reportTestCases {
		t.Run(tc.reportTestName, func(t *testing.T) {
			log := &testdata.TestLogger{T: t}

			if err := app.Run(tc.pmdcpdFile, tc.codeQualityFile, log); err != nil {
				t.Fatalf("Expected no error, but got %v", err)
			}

			actualCodeQualityReport, err := files.ReadFileToBytes(tc.codeQualityFile)
			if err != nil {
				t.Fatalf("Error reading file: %s", tc.codeQualityFile)
			}
			expectedCodeQualityReport, err := files.ReadFileToBytes(tc.expCodeQualityFile)
			if err != nil {
				t.Fatalf("Error reading file: %s", tc.expCodeQualityFile)
			}

			if string(actualCodeQualityReport) != string(expectedCodeQualityReport) {
				t.Errorf("Expected Code Quality report:\n%s\nGot:\n%s\n",
					string(expectedCodeQualityReport), string(actualCodeQualityReport))
			}
		})
	}
}
