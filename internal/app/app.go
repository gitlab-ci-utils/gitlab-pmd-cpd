package app

import (
	"gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/internal/codequality"
	"gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/internal/files"
	"gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/internal/logger"
	"gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/internal/pmdcpd"
)

func Run(pmdcpdFilename string, codeQualityFilename string, log logger.Logger) error {
	data, err := files.ReadFileToBytes(pmdcpdFilename)
	if err != nil {
		log.Printf("Error reading file: %s", pmdcpdFilename)
		return err
	}
	log.Println("Read file:", pmdcpdFilename)

	pmdcpd, err := pmdcpd.UnmarshalPmdCpdResults(data)
	if err != nil {
		log.Printf("Error extracting PMD CPD results from file: %s", pmdcpdFilename)
		return err
	}
	log.Printf("PMD CPD analyzed %d files, found %d duplications",
		len(pmdcpd.Files), len(pmdcpd.Duplications))

	log.Printf("Converting file %s to GitLab Code Quality JSON", pmdcpdFilename)
	codeQualityResults := codequality.ConvertPmdCpdToCodeQuality(pmdcpd)
	codeQualityJSON := codequality.MarshallCodeQualityReport(codeQualityResults)

	if err = files.WriteBytesToFile(codeQualityFilename, codeQualityJSON); err != nil {
		log.Println("Error writing file:", codeQualityFilename)
		return err
	}
	log.Println("GitLab Code Quality report saved to file:", codeQualityFilename)

	return nil
}
