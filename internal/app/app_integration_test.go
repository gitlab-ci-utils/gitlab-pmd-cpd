package app_test

import (
	"os/exec"
	"strings"
	"testing"
)

func runWithArgs(args ...string) (string, error) {
	cmdName := "go"
	baseCmdArgs := []string{"run", "../../cmd/gitlab-pmd-cpd/"}
	//nolint:gocritic // Intended to assign to a new slice.
	cmdArgs := append(baseCmdArgs, args...)

	cmd := exec.Command(cmdName, cmdArgs...)
	var out strings.Builder
	cmd.Stdout = &out
	cmd.Stderr = &out

	err := cmd.Run()
	return out.String(), err
}

type cliTestCase struct {
	name     string
	args     []string
	result   string
	hasError bool
}

func TestIntegration(t *testing.T) {
	testCases := []cliTestCase{
		{
			name:     "should create Code Quality report if provided a file with no duplicates",
			args:     []string{"../testdata/pmdcpd/no-duplication.xml"},
			result:   "report saved to file: ../testdata/pmdcpd/no-duplication.json",
			hasError: false,
		},
		{
			name:     "should create Code Quality report if provided a file with multiple duplicates",
			args:     []string{"../testdata/pmdcpd/multiple-duplications.xml"},
			result:   "report saved to file: ../testdata/pmdcpd/multiple-duplications.json",
			hasError: false,
		},
		{
			name:     "should exit with error if file name not provided",
			args:     nil,
			result:   "report file name must be specified as the first argument",
			hasError: true,
		},
		{
			name:     "should exit with error if provided an invalid input file type",
			args:     []string{"../../README.md"},
			result:   "file must be XML",
			hasError: true,
		},
		{
			name:     "should exit with error if provided a non-existent file name",
			args:     []string{"foo.xml"},
			result:   "no such file or directory",
			hasError: true,
		},
		{
			name:     "should exit with error if provided a file with invalid XML",
			args:     []string{"../testdata/pmdcpd/invalid-xml.xml"},
			result:   "XML syntax error ",
			hasError: true,
		},
		{
			name:     "should exit with error if provided a file with invalid XML schema",
			args:     []string{"../testdata/pmdcpd/other-schema.xml"},
			result:   "expected element type <pmd-cpd>",
			hasError: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			output, err := runWithArgs(tc.args...)

			if tc.hasError && err == nil {
				t.Fatalf("Expected error, but got none")
			} else if !tc.hasError && err != nil {
				t.Fatalf("Expected no error, but got %v", err)
			}

			if !strings.Contains(output, tc.result) {
				t.Errorf("Expected error to contain \"%s\", but got:\n%s\n", tc.result, output)
			}
		})
	}
}
