package logger

//nolint:depguard // Will move to slog with #21.
import "log"

type Logger interface {
	Fatalln(v ...interface{})
	Printf(format string, v ...interface{})
	Println(v ...interface{})
}

type DefaultLogger struct{}

func (l *DefaultLogger) Fatalln(v ...interface{}) {
	log.Fatalln(v...)
}

func (l *DefaultLogger) Printf(format string, v ...interface{}) {
	log.Printf(format, v...)
}

func (l *DefaultLogger) Println(v ...interface{}) {
	log.Println(v...)
}
