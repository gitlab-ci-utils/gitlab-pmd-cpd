package codequality

import (
	"bytes"
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/internal/pmdcpd"
)

type Issue struct {
	Categories     []string       `json:"categories"`
	CheckName      string         `json:"check_name"`
	Content        ContentInfo    `json:"content"`
	Description    string         `json:"description"`
	EngineName     string         `json:"engine_name"`
	Severity       string         `json:"severity"`
	Type           string         `json:"type"`
	Fingerprint    string         `json:"fingerprint"`
	Location       LocationInfo   `json:"location"`
	OtherLocations []LocationInfo `json:"other_locations"`
}

type DuplicateLocationInfo struct {
	Location       LocationInfo   `json:"location"`
	OtherLocations []LocationInfo `json:"other_locations"`
}

type ContentInfo struct {
	Body   string `json:"body"`
	Group  int    `json:"group"`
	Tokens int    `json:"tokens"`
}
type LocationInfo struct {
	Path      string        `json:"path"`
	Positions PositionsInfo `json:"positions"`
}

type PositionsInfo struct {
	Begin BeginEndInfo `json:"begin"`
	End   BeginEndInfo `json:"end"`
}

type BeginEndInfo struct {
	Column int `json:"column"`
	Line   int `json:"line"`
}

// getIssueDefaults returns a Code Quality Issue with default values.
func getIssueDefaults() Issue {
	return Issue{
		Categories: []string{
			"Duplication",
		},
		CheckName:  "duplicate-code",
		EngineName: "pmd-cpd",
		Severity:   "major",
		Type:       "issue",
	}
}

// getIssueDescription returns a description for the Code Quality Issue based
// on the number of duplications.
func getIssueDescription(duplication pmdcpd.Duplication, dupIndex int) string {
	return fmt.Sprintf("Duplicate code found in %d locations (Duplication %d). Consider refactoring.",
		len(duplication.Files), dupIndex+1)
}

// getIssueFingerprint returns a sha256 hash of duplicated code fragment.
func getIssueFingerprint(duplication pmdcpd.Duplication, fileIndex int) string {
	h := sha256.New()
	if _, err := h.Write([]byte(duplication.CodeFragment)); err != nil {
		// This error is included to satisfy the  io.Writer interface and
		// should never occur, so panic.
		panic(err)
	}
	return fmt.Sprintf("%x-%d", h.Sum(nil), fileIndex+1)
}

// normalizePath normalizes the path to a POSIX relative path from the current
// working directory.
func normalizePath(path string) string {
	baseDirectory := os.Getenv("CI_PROJECT_DIR")
	if baseDirectory == "" {
		baseDirectory, _ = os.Getwd()
	}

	// Normalize the path (clean up `..`, `.`, `//`, etc.)
	normalizedPath := filepath.Clean(path)
	// Convert to POSIX path separators
	posixPath := filepath.ToSlash(normalizedPath)
	// Remove the base directory to have a relative path
	relativePath := strings.TrimPrefix(posixPath, baseDirectory)
	// Remove any leading /
	return strings.TrimPrefix(relativePath, "/")
}

// getIssueLocation returns the location data for the Code Quality Issue.
func getIssueLocation(duplication pmdcpd.FileDuplication) LocationInfo {
	return LocationInfo{
		Path: normalizePath(duplication.Path),
		Positions: PositionsInfo{
			Begin: BeginEndInfo{
				Line:   duplication.BeginLine,
				Column: duplication.BeginColumn,
			},
			End: BeginEndInfo{
				Line:   duplication.EndLine,
				Column: duplication.EndColumn,
			},
		},
	}
}

// getDuplicateLocations returns the various combinations of duplicates
// (locations, other locations) for the Code Quality Issue.
func getDuplicateLocations(duplication pmdcpd.Duplication) []DuplicateLocationInfo {
	locations := []LocationInfo{}
	for _, file := range duplication.Files {
		locations = append(locations, getIssueLocation(file))
	}

	fileCount := len(locations)
	duplicateLocations := []DuplicateLocationInfo{}

	for i := range fileCount {
		duplicateLocation := DuplicateLocationInfo{}
		for fileIndex, location := range locations {
			if fileIndex == i {
				duplicateLocation.Location = location
			} else {
				duplicateLocation.OtherLocations = append(duplicateLocation.OtherLocations, location)
			}
		}
		duplicateLocations = append(duplicateLocations, duplicateLocation)
	}
	return duplicateLocations
}

// ConvertPmdCpdToCodeQuality converts the PMD CPD results to GitLab Code
// Quality results.
func ConvertPmdCpdToCodeQuality(pmdcpdResults pmdcpd.PMDCPD) []Issue {
	codeQualityIssues := []Issue{}
	for dupIndex, duplication := range pmdcpdResults.Duplications {
		// Generate issueBase with values that are not dependent on the
		// duplicate location
		issueBase := getIssueDefaults()
		issueBase.Content = ContentInfo{
			Body:   duplication.CodeFragment,
			Group:  dupIndex + 1,
			Tokens: duplication.Tokens,
		}
		issueBase.Description = getIssueDescription(duplication, dupIndex)

		// Get all possible combinations of duplicate locations, and for each
		// add a copy of the issue to the results
		duplicateLocations := getDuplicateLocations(duplication)
		for locIndex, dupLocation := range duplicateLocations {
			newIssue := issueBase
			newIssue.Location = dupLocation.Location
			newIssue.OtherLocations = dupLocation.OtherLocations
			newIssue.Fingerprint = getIssueFingerprint(duplication, locIndex)

			codeQualityIssues = append(codeQualityIssues, newIssue)
		}
	}
	return codeQualityIssues
}

// MarshallCodeQualityReport marshalls the Code Quality report to JSON
// conforming to the GitLab Code Quality report schema.
func MarshallCodeQualityReport(issues []Issue) []byte {
	// Use new encoder to marshal, which can be set to not escape HTML
	var buf bytes.Buffer
	enc := json.NewEncoder(&buf)
	enc.SetEscapeHTML(false)

	if err := enc.Encode(issues); err != nil {
		// Encode should never return an error for a valid schema, which has
		// no floats that can have invalid values, so panic
		panic(err)
	}

	return buf.Bytes()
}
