//nolint:cyclop,funlen,gocognit // Don't test function length or complexity in tests
package codequality_test

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"testing"

	"gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/internal/codequality"
	"gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/internal/pmdcpd"
	"gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/internal/testdata"
)

func TestConvertPmdCpdToCodeQuality(t *testing.T) {
	t.Setenv("CI_PROJECT_DIR", "/builds/gitlab-ci-utils/gitlab-pmd-cpd")
	codeQualityIssues := codequality.ConvertPmdCpdToCodeQuality(testdata.MultipleDuplications)

	t.Run("Returns Code Quality Issue values", func(t *testing.T) {
		t.Run("With `categories` set to `Duplication`", func(t *testing.T) {
			if codeQualityIssues[0].Categories[0] != "Duplication" {
				t.Errorf("Expected categories to be `Duplication`, got %s",
					codeQualityIssues[0].Categories[0])
			}
		})

		t.Run("With `check_name` set to `duplicate-code`", func(t *testing.T) {
			if codeQualityIssues[0].CheckName != "duplicate-code" {
				t.Errorf("Expected check_name to be `duplicate-code`, got %s",
					codeQualityIssues[0].CheckName)
			}
		})

		t.Run("With `content.body` set to the duplicated code`", func(t *testing.T) {
			if codeQualityIssues[0].Content.Body != testdata.MultipleDuplications.Duplications[0].CodeFragment {
				t.Errorf("Expected content.body to be the duplicated code, got %s",
					codeQualityIssues[0].Content.Body)
			}
		})

		t.Run("With `content.group` set to the duplication group", func(t *testing.T) {
			expectedValues := map[int]int{
				0: 1, // First duplication group has 2 locations
				2: 2, // Second duplication group has 3 locations
				5: 3,
			}
			for key, value := range expectedValues {
				if codeQualityIssues[key].Content.Group != value {
					t.Errorf("Expected content.group to be %d, got %d", value, codeQualityIssues[key].Content.Group)
				}
			}
		})

		t.Run("With `content.tokens` set to the number of tokens", func(t *testing.T) {
			if codeQualityIssues[0].Content.Tokens != testdata.MultipleDuplications.Duplications[0].Tokens {
				t.Errorf("Expected content.tokens to be %d, got %d",
					testdata.MultipleDuplications.Duplications[0].Tokens,
					codeQualityIssues[0].Content.Tokens)
			}
		})

		t.Run("With `description` indicating the number of duplications", func(t *testing.T) {
			expectedDescription :=
				fmt.Sprintf("%d locations", len(testdata.MultipleDuplications.Duplications[0].Files))
			if !strings.Contains(codeQualityIssues[0].Description, expectedDescription) {
				t.Errorf("Expected description to contain %s, got %s",
					expectedDescription,
					codeQualityIssues[0].Description)
			}
		})

		t.Run("With `description` indicating the duplication group", func(t *testing.T) {
			issueIndex := 0
			expectedDescription := fmt.Sprintf("Duplication %d", issueIndex+1)
			if !strings.Contains(codeQualityIssues[issueIndex].Description, expectedDescription) {
				t.Errorf("Expected description to contain %s, got %s",
					expectedDescription,
					codeQualityIssues[0].Description)
			}
		})

		t.Run("With `engine_name` set to `pmd-cpd`", func(t *testing.T) {
			if codeQualityIssues[0].EngineName != "pmd-cpd" {
				t.Errorf("Expected engine_name to be `pmd-cpd`, got %s", codeQualityIssues[0].EngineName)
			}
		})

		t.Run("With `severity` set to `major`", func(t *testing.T) {
			if codeQualityIssues[0].Severity != "major" {
				t.Errorf("Expected severity to be `major`, got %s", codeQualityIssues[0].Severity)
			}
		})

		t.Run("With `type` set to `issue`", func(t *testing.T) {
			if codeQualityIssues[0].Type != "issue" {
				t.Errorf("Expected type to be `issue`, got %s", codeQualityIssues[0].Type)
			}
		})

		t.Run("With `fingerprint` set to a valid sha256 hash with index", func(t *testing.T) {
			// A valid SHA256 hash is a hexadecimal number that is exactly 64 characters long.
			matched, err := regexp.MatchString("^[a-fA-F0-9]{64}-\\d+$", codeQualityIssues[0].Fingerprint)
			if err != nil {
				t.Error("Unexpected error matching fingerprint format", err)
			}

			if matched != true {
				t.Errorf("Expected fingerprint to be `<sha256 hash>-<group>`, got %s", codeQualityIssues[0].Fingerprint)
			}
		})

		t.Run("With `location` set to first file location", func(t *testing.T) {
			if !testdata.AreEquivalentLocations(testdata.MultipleDuplications.Duplications[0].Files[0],
				codeQualityIssues[0].Location) {
				t.Errorf("Expected location to be %v, got %v",
					testdata.MultipleDuplications.Duplications[0].Files[0],
					codeQualityIssues[0].Location)
			}
		})

		t.Run("With `other_locations` set to the other file locations", func(t *testing.T) {
			if len(codeQualityIssues[0].OtherLocations) != 1 {
				t.Errorf("Expected other_locations to have 1 entry, got %d",
					len(codeQualityIssues[0].OtherLocations))
			}

			if !testdata.AreEquivalentLocations(testdata.MultipleDuplications.Duplications[0].Files[1],
				codeQualityIssues[0].OtherLocations[0]) {
				t.Errorf("Expected location to be %v, got %v",
					testdata.MultipleDuplications.Duplications[0].Files[0],
					codeQualityIssues[0].Location)
			}
		})
	})

	t.Run("Returns Code Quality issues duplicated for each location", func(t *testing.T) {
		index := 0
		for dupIndex, duplication := range testdata.MultipleDuplications.Duplications {
			fingerprints := []string{}
			for fileIndex, file := range duplication.Files {
				testName := fmt.Sprintf("With correct data for duplication %d location %d", dupIndex+1, fileIndex+1)
				t.Run(testName, func(t *testing.T) {
					// With `Content.Body` matching the duplicated code
					if codeQualityIssues[index].Content.Body != duplication.CodeFragment {
						t.Errorf("Expected content.body to be '%s', got '%s'",
							duplication.CodeFragment,
							codeQualityIssues[index].Content.Body)
					}

					// With `Location` matching the first file location
					if !testdata.AreEquivalentLocations(file, codeQualityIssues[index].Location) {
						t.Errorf("Expected location to be %v, got %v",
							file,
							codeQualityIssues[index].Location)
					}

					// With `OtherLocations` length matching the number of other file locations
					if len(duplication.Files)-1 != len(codeQualityIssues[index].OtherLocations) {
						t.Errorf("Expected other_locations to have %d entries, got %d",
							len(duplication.Files)-1,
							len(codeQualityIssues[index].OtherLocations))
					}
				})

				// Collect fingerprints to compare all for this duplication
				fingerprints = append(fingerprints, codeQualityIssues[index].Fingerprint)
				index++
			}

			t.Run("With correct `Fingerprint` base and suffix for all locations", func(t *testing.T) {
				var baseFingerprint string
				for fpIndex, fingerprint := range fingerprints {
					fingerprintIndex := strings.Split(fingerprint, "-")
					if fpIndex == 0 {
						baseFingerprint = fingerprintIndex[0]
						if fingerprintIndex[1] != strconv.Itoa(fpIndex+1) {
							t.Errorf("Expected fingerprint suffix to be '-%d', got '-%s'",
								fpIndex+1,
								fingerprintIndex[1])
						}
					} else {
						if baseFingerprint != fingerprintIndex[0] {
							t.Errorf("Expected base fingerprint to be '%s', got '%s'",
								baseFingerprint,
								fingerprintIndex[0])
						}

						if fingerprintIndex[1] != strconv.Itoa(fpIndex+1) {
							t.Errorf("Expected fingerprint suffix to be '-%d', got '-%s'",
								fpIndex+1,
								fingerprintIndex[1])
						}
					}
				}
			})
		}
	})

	t.Run("Returns zero length Code Quality issues slice for no duplicates", func(t *testing.T) {
		issues := codequality.ConvertPmdCpdToCodeQuality(pmdcpd.PMDCPD{})

		if len(issues) != 0 {
			t.Errorf("Expected no issues, got %d", len(issues))
		}
	})
}

func TestMarshallCodeQualityReport(t *testing.T) {
	t.Setenv("CI_PROJECT_DIR", "/builds/gitlab-ci-utils/gitlab-pmd-cpd")
	codeQualityIssues := codequality.ConvertPmdCpdToCodeQuality(testdata.MultipleDuplications)

	report := codequality.MarshallCodeQualityReport(codeQualityIssues)
	if !testdata.IsValidJSONArray(report) {
		t.Error("Expected marshalled code quality report to be valid JSON")
	}
}
