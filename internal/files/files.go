package files

import (
	"io/fs"
	"os"
)

// ReadFileToBytes reads a file with the given name and returns a slice of bytes containing its contents.
func ReadFileToBytes(filename string) ([]byte, error) {
	// User input required to specify input file.
	// nosemgrep: gosec.G304-1
	data, err := os.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return data, nil
}

// WriteBytesToFile writes a slice of bytes to a file with the given name.
func WriteBytesToFile(filename string, data []byte) error {
	var defaultFileMode fs.FileMode = 0600
	if err := os.WriteFile(filename, data, defaultFileMode); err != nil {
		return err
	}
	return nil
}
