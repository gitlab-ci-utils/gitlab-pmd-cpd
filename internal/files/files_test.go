package files_test

import (
	"os"
	"reflect"
	"testing"

	"gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/internal/files"
)

type testFileError struct {
	testName string
	file     string
}

func TestFiles_ReadFileToBytes(t *testing.T) {
	t.Run("Returns slice of bytes", func(t *testing.T) {
		testFiles := []testFileError{
			{
				testName: "For file with valid XML",
				file:     "../testdata/pmdcpd/pmd-cpd-results.xml",
			},
			{
				testName: "For empty file",
				file:     "../testdata/pmdcpd/empty-file.xml",
			},
		}

		for _, testFile := range testFiles {
			t.Run(testFile.testName, func(t *testing.T) {
				data, err := files.ReadFileToBytes(testFile.file)
				if data == nil {
					t.Errorf("Expected data, got nil")
				}

				if err != nil {
					t.Errorf("Expected no error, got %v", err)
				}
			})
		}
	})

	t.Run("Returns an error", func(t *testing.T) {
		testFiles := []testFileError{
			{
				testName: "For filename that does not exit",
				file:     "../testdata/pmdcpd/file-does-not-exist.xml",
			},
			{
				testName: "For filename that is a directory",
				file:     "../testdata/pmdcpd/",
			},
		}

		for _, testFile := range testFiles {
			t.Run(testFile.testName, func(t *testing.T) {
				_, err := files.ReadFileToBytes(testFile.file)
				if err == nil {
					t.Errorf("Expected error, got nil")
				}
			})
		}
	})
}

func TestWriteBytesToFile(t *testing.T) {
	testData := []byte("test data")
	t.Run("Writes file from slice of bytes", func(t *testing.T) {
		testFiles := []testFileError{
			{
				testName: "To given filename with given data",
				file:     "../testdata/test.json",
			},
		}

		for _, testFile := range testFiles {
			t.Run(testFile.testName, func(t *testing.T) {
				err := files.WriteBytesToFile(testFile.file, testData)
				if err != nil {
					t.Errorf("Expected no error, got %v", err)
				}

				// Data driven test with filename specified above.
				// nosemgrep: gosec.G304-1
				data, err := os.ReadFile(testFile.file)
				if err != nil {
					t.Errorf("File %s was not created: %v", testFile.file, err)
				}

				if reflect.DeepEqual(data, testData) == false {
					t.Error("Expected data was not written to file")
				}
			})
		}
	})

	t.Run("Returns an error", func(t *testing.T) {
		testFiles := []testFileError{
			{
				testName: "For file path that does not exit",
				file:     "../dir-that-does-not-exist/test.json",
			},
		}

		for _, testFile := range testFiles {
			t.Run(testFile.testName, func(t *testing.T) {
				err := files.WriteBytesToFile(testFile.file, testData)
				if err == nil {
					t.Error("Expected error, got nil")
				}
			})
		}
	})
}
