package pmdcpd

import (
	"encoding/xml"
)

type File struct {
	Path   string `xml:"path,attr"`
	Tokens int    `xml:"totalNumberOfTokens,attr"`
}

type FileDuplication struct {
	BeginLine   int    `xml:"line,attr"`
	BeginColumn int    `xml:"column,attr"`
	BeginToken  int    `xml:"begintoken,attr"`
	EndLine     int    `xml:"endline,attr"`
	EndColumn   int    `xml:"endcolumn,attr"`
	EndToken    int    `xml:"endtoken,attr"`
	Path        string `xml:"path,attr"`
}

type Duplication struct {
	Lines        int               `xml:"lines,attr"`
	Tokens       int               `xml:"tokens,attr"`
	Files        []FileDuplication `xml:"file"`
	CodeFragment string            `xml:"codefragment"`
}

type PMDCPD struct {
	XMLName      xml.Name      `xml:"pmd-cpd"`
	Files        []File        `xml:"file"`
	Duplications []Duplication `xml:"duplication"`
}

// UnmarshalPmdCpdResults unmarshals the PMD CPD XML report into a
// PMDCPD struct.
func UnmarshalPmdCpdResults(xmlData []byte) (PMDCPD, error) {
	var pmd PMDCPD
	if err := xml.Unmarshal(xmlData, &pmd); err != nil {
		return PMDCPD{}, err
	}
	return pmd, nil
}
