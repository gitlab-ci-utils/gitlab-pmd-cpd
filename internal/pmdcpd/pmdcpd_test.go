//nolint:gocognit,cyclop // Don't test complexity in tests
package pmdcpd_test

import (
	"reflect"
	"testing"

	"gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/internal/files"
	"gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/internal/pmdcpd"
	"gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/internal/testdata"
)

type testFileError struct {
	testName string
	file     string
	dupCount int
	data     []byte
}

func TestUnmarshalPmdCpdResults(t *testing.T) {
	t.Run("Returns PMDCPD struct", func(t *testing.T) {
		testFiles := []testFileError{
			{
				testName: "For data with no duplications",
				file:     "../testdata/pmdcpd/no-duplication.xml",
				dupCount: 0,
			},
			{
				testName: "For data with a single duplication",
				file:     "../testdata/pmdcpd/single-duplication.xml",
				dupCount: 1,
			},
			{
				testName: "For data with multiple duplications",
				file:     "../testdata/pmdcpd/multiple-duplications.xml",
				dupCount: 3,
			},
		}

		for _, testFile := range testFiles {
			t.Run(testFile.testName, func(t *testing.T) {
				data, err := files.ReadFileToBytes(testFile.file)
				if err != nil {
					t.Fatal("Error reading file:", testFile.file)
				}
				pmdcpd, err := pmdcpd.UnmarshalPmdCpdResults(data)
				if err != nil {
					t.Error("Unexpected error extracting PMD CPD results from file: ", testFile.file)
				}
				if len(pmdcpd.Duplications) != testFile.dupCount {
					t.Errorf("Expected %d duplications, got %d", testFile.dupCount, len(pmdcpd.Duplications))
				}
			})
		}

		t.Run("With valid results for data with multiple duplications", func(t *testing.T) {
			filename := "../testdata/pmdcpd/multiple-duplications.xml"
			data, err := files.ReadFileToBytes(filename)
			if err != nil {
				t.Fatal("Error reading file:", filename)
			}
			pmdcpd, err := pmdcpd.UnmarshalPmdCpdResults(data)
			if err != nil {
				t.Error("Unexpected error extracting PMD CPD results from file: ", filename)
			}
			if reflect.DeepEqual(pmdcpd, testdata.MultipleDuplications) == false {
				t.Errorf("\nExpected:\n%+v\nGot\n%+v\n", testdata.MultipleDuplications, pmdcpd)
			}
		})
	})

	t.Run("Returns an error", func(t *testing.T) {
		invalidSchemaFile := "../testdata/pmdcpd/other-schema.xml"
		invalidSchemaData, fileErr := files.ReadFileToBytes(invalidSchemaFile)
		if fileErr != nil {
			t.Fatal("Error reading file:", invalidSchemaFile)
		}

		testFiles := []testFileError{
			{
				testName: "For empty data",
				data:     []byte{},
			},
			{
				testName: "For invalid XML",
				data:     []byte("<foo>"),
			},
			{
				testName: "For data with another xml schema",
				data:     invalidSchemaData,
			},
		}

		for _, testFile := range testFiles {
			t.Run(testFile.testName, func(t *testing.T) {
				_, err := pmdcpd.UnmarshalPmdCpdResults(testFile.data)
				if err == nil {
					t.Errorf("Expected error, got nil")
				}
			})
		}
	})
}
