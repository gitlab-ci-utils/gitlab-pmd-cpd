package testdata

import (
	"encoding/xml"

	"gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/internal/pmdcpd"
)

// Expected results from the multiple-duplications.xml file.
var MultipleDuplications pmdcpd.PMDCPD = pmdcpd.PMDCPD{
	XMLName: xml.Name{
		Space: "https://pmd-code.org/schema/cpd-report",
		Local: "pmd-cpd",
	},
	Files: []pmdcpd.File{
		{
			Path:   "/builds/gitlab-ci-utils/gitlab-pmd-cpd/internal/testdata/exampleproject/file2.js",
			Tokens: 95,
		},
		{
			Path:   "/builds/gitlab-ci-utils/gitlab-pmd-cpd/internal/testdata/exampleproject/file3.js",
			Tokens: 35,
		},
		{
			Path:   "/builds/gitlab-ci-utils/gitlab-pmd-cpd/internal/testdata/exampleproject/file4.js",
			Tokens: 210,
		},
		{
			Path:   "/builds/gitlab-ci-utils/gitlab-pmd-cpd/internal/testdata/exampleproject/file5.js",
			Tokens: 168},
	},
	Duplications: []pmdcpd.Duplication{
		{
			Lines:  12,
			Tokens: 68,
			CodeFragment: "    const a = [1, 3, 5];\n" +
				"    console.log(`total: ${a.length}`);\n" +
				"    for (let i = 0; i < a.length; i++) {\n" +
				"        if (a[i] < 5) {\n" +
				"            console.log(a[i]);\n" +
				"        }\n" +
				"        // output error if value equal to 5\n" +
				"        if (a[i] === 5) {\n" +
				"            console.log('error');\n" +
				"        }\n" +
				"    }\n" +
				"};\n",
			Files: []pmdcpd.FileDuplication{
				{
					BeginLine:   2,
					BeginColumn: 23,
					BeginToken:  360,
					EndLine:     13,
					EndColumn:   3,
					EndToken:    427,
					Path:        "/builds/gitlab-ci-utils/gitlab-pmd-cpd/internal/testdata/exampleproject/file5.js",
				},
				{
					BeginLine:   16,
					BeginColumn: 20,
					BeginToken:  443,
					EndLine:     27,
					EndColumn:   3,
					EndToken:    510,
					Path:        "/builds/gitlab-ci-utils/gitlab-pmd-cpd/internal/testdata/exampleproject/file5.js",
				},
			},
		},
		{
			Lines:  4,
			Tokens: 17,
			CodeFragment: "    if (badWords.includes(value)) {\n" +
				"        console.log('***');\n" +
				"    } else {\n" +
				"        console.log(value);\n",
			Files: []pmdcpd.FileDuplication{
				{
					BeginLine:   8,
					BeginColumn: 32,
					BeginToken:  169,
					EndLine:     11,
					EndColumn:   21,
					EndToken:    185,
					Path:        "/builds/gitlab-ci-utils/gitlab-pmd-cpd/internal/testdata/exampleproject/file4.js",
				},
				{
					BeginLine:   16,
					BeginColumn: 32,
					BeginToken:  208,
					EndLine:     20,
					EndColumn:   21,
					EndToken:    224,
					Path:        "/builds/gitlab-ci-utils/gitlab-pmd-cpd/internal/testdata/exampleproject/file4.js",
				},
				{
					BeginLine:   25,
					BeginColumn: 36,
					BeginToken:  247,
					EndLine:     28,
					EndColumn:   21,
					EndToken:    263,
					Path:        "/builds/gitlab-ci-utils/gitlab-pmd-cpd/internal/testdata/exampleproject/file4.js",
				},
			},
		},
		{
			Lines:  5,
			Tokens: 15,
			CodeFragment: "        console.log(`value: ${value}`);\n" +
				"    } else {\n" +
				"        console.log('nada');\n" +
				"    }\n" +
				"};\n",
			Files: []pmdcpd.FileDuplication{
				{
					BeginLine:   14,
					BeginColumn: 38,
					BeginToken:  80,
					EndLine:     18,
					EndColumn:   3,
					EndToken:    94,
					Path:        "/builds/gitlab-ci-utils/gitlab-pmd-cpd/internal/testdata/exampleproject/file2.js",
				},
				{
					BeginLine:   3,
					BeginColumn: 34,
					BeginToken:  116,
					EndLine:     7,
					EndColumn:   3,
					EndToken:    130,
					Path:        "/builds/gitlab-ci-utils/gitlab-pmd-cpd/internal/testdata/exampleproject/file3.js",
				},
			},
		},
	},
}
