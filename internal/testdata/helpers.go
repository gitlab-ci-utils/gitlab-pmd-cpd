package testdata

import (
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/internal/codequality"
	"gitlab.com/gitlab-ci-utils/gitlab-pmd-cpd/internal/pmdcpd"
)

func AreEquivalentLocations(expected pmdcpd.FileDuplication, actual codequality.LocationInfo) bool {
	return strings.Contains(expected.Path, actual.Path) &&
		actual.Positions.Begin.Line == expected.BeginLine &&
		actual.Positions.Begin.Column == expected.BeginColumn &&
		actual.Positions.End.Line == expected.EndLine &&
		actual.Positions.End.Column == expected.EndColumn
}

func IsValidJSONArray(b []byte) bool {
	var js []interface{}
	return json.Unmarshal(b, &js) == nil
}

func AreEqualStringSlices(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

type TestLogger struct {
	T        *testing.T
	FatalLog []string
	PrintLog []string
}

func getFormattedString(format string, v ...interface{}) string {
	return strings.TrimSpace(fmt.Sprintf(format, v...))
}

func getString(v ...interface{}) string {
	return strings.TrimSpace(fmt.Sprintln(v...))
}

func (l *TestLogger) Fatalln(v ...interface{}) {
	l.FatalLog = append(l.FatalLog, getString(v...))
}

func (l *TestLogger) Printf(format string, v ...interface{}) {
	l.PrintLog = append(l.PrintLog, getFormattedString(format, v...))
}

func (l *TestLogger) Println(v ...interface{}) {
	l.PrintLog = append(l.PrintLog, getString(v...))
}
