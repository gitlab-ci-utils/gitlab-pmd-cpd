export const aFunction = (foo) => {
    if (foo) {
        console.log(`foo: ${foo}`);
    } else {
        console.log('nada');
    }
};
