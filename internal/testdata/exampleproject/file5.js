const function1 = () => {
    const a = [1, 3, 5];
    console.log(`total: ${a.length}`);
    for (let i = 0; i < a.length; i++) {
        if (a[i] < 5) {
            console.log(a[i]);
        }
        // output error if value equal to 5
        if (a[i] === 5) {
            console.log('error');
        }
    }
};

const function2 = () => {
    const a = [2, 4];
    console.log(`total: ${a.length}`);
    for (let i = 0; i < a.length; i++) {
        // log if less than 5
        if (a[i] < 5) {
            console.log(a[i]);
        }
        if (a[i] === 5) {
            console.log('error');
        }
    }
};
