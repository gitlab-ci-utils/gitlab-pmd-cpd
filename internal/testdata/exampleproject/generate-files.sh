#!/bin/bash

testdir="./internal/testdata"

function run_pmdcpd {
  pmd cpd --dir="$testdir/exampleproject/" --minimum-tokens="$1" \
    --language="ecmascript" --no-fail-on-violation --skip-lexical-errors=true \
    --format="xml" > "$testdir/pmdcpd/$2"
}

run_pmdcpd 100 no-duplication.xml
run_pmdcpd 50 single-duplication.xml
run_pmdcpd 15 multiple-duplications.xml
