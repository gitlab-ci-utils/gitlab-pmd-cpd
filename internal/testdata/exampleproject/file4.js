'use strict';

const fs = require('fs');

const badWords = ['badword', 'badword2', 'badword3'];

const myFunction = (value) => {
    if (badWords.includes(value)) {
        console.log('***');
    } else {
        console.log(value);
    }
};

const myFunction2 = (input) => {
    if (badWords.includes(input)) {
        // This is the same as the previous one
        console.log('***');
    } else {
        console.log(input);
    }
};

const myFunction3 = (something) => {
    if (badWords.includes(something)) {
        console.log('***');
    } else {
        console.log(something);
    }
};

const readFile = (fileName) => {
    const data = fs.readFileSync(fileName, 'utf8');
    return JSON.parse(data);
};

const readFileFromString = (fileName) => {
    const normalizedPath = path.normalize(fileName);
    const data = fs.readFileSync(normalizedPath, 'utf8');
    return JSON.parse(data);
};
