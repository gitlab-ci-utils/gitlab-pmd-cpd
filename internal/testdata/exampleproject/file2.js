export const someFunction = (bar, baz) => {
    if (bar && baz) {
        if (bar > baz) {
            console.log(`foo: ${bar}`);
        } else if (bar < baz) {
            console.log(`bar: ${baz}`);
        }
    }
    console.log('nada');
};

export const anotherFunction = (value) => {
    if (value) {
        console.log(`value: ${value}`);
    } else {
        console.log('nada');
    }
};
