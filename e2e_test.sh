#!/bin/sh

process_test_result() {
  if [ "$1" = "pass" ]; then
    printf " - PASS\n"
  else
    printf " - FAIL\n"
    tests_failed=true
  fi
}

test_pass() {
  printf "%s" "$1"
  result=$($2 > /dev/null 2>&1 && echo "pass" || echo "fail")
  process_test_result "$result"
}

test_fail() {
  printf "%s" "$1"
  result=$($2 > /dev/null 2>&1 && echo "fail" || echo "pass")
  process_test_result "$result"
}

# Setup test configuration
tests_failed=false
mkdir -p coverage
export GOCOVERDIR=coverage
go build -cover -covermode=count -o gitlab-pmd-cpd-cover ./cmd/gitlab-pmd-cpd/

# Execute end-to-end tests
printf "\nEnd-to-end tests:\n"

test_case_1="Should exit 1 if no input file specified"
test_fail "$test_case_1" "./gitlab-pmd-cpd-cover"

test_case_2="Should exit 1 if non-existent file specified"
test_fail "$test_case_2" "./gitlab-pmd-cpd-cover foo.xml"

test_case_3="Should exit 1 if non-XML file specified"
touch foo.json
test_fail "$test_case_3" "./gitlab-pmd-cpd-cover foo.json"

test_case_4="Should exit 1 if invalid XML file specified"
touch foo.xml
test_fail "$test_case_4" "./gitlab-pmd-cpd-cover foo.xml"

test_case_5="Should process file and exit 0 if valid PMD CPD file specified and CI_PROJECT_DIR set"
if [ -z "$CI_PROJECT_DIR" ]; then CI_PROJECT_DIR="$(pwd)"; export CI_PROJECT_DIR; fi
test_pass "$test_case_5" "./gitlab-pmd-cpd-cover ./internal/testdata/pmdcpd/single-duplication.xml"

test_case_6="Should process file and exit 0 if valid PMD CPD file specified and CI_PROJECT_DIR not set"
ci_project_dir_orig="$CI_PROJECT_DIR"
unset CI_PROJECT_DIR
test_pass "$test_case_6" "./gitlab-pmd-cpd-cover ./internal/testdata/pmdcpd/single-duplication.xml"
export CI_PROJECT_DIR="$ci_project_dir_orig"

# Generate -coverprofile formatted coverage file
go tool covdata textfmt -i=./coverage -o coverage-e2e.out

# Fail job if any test failed
if [ "$tests_failed" = true ]; then exit 1; fi
